# -*- coding: utf-8 -*-

# Use standard python package only.
import random
import string
'''
1 = sumOfOnes (min)
2 = sumOfOnes (Max)
3 = semanticOptimisation
4 = randomNumbers 
5 = ???
'''
whichProblem = 4  # 1 = sum of 1s

# Varun variables
POPULATION_SIZE = 10
GENERATIONS = 200
CROSSOVER_RATE = 0.8
MUTATION_RATE = 0.2
SELECTION_METHOD = 1

# Kai variations 2
lowerBound = 0
upperBound = 1
numberOfGenes = 6
mutationMethod = 1
crossoverMethod = 1
finalGenerationCount = -1
semanticPhrase = "cat"
optimalNumber = []

solutionFound = False

def sumOfOnesMin(individual):
    fitness = 0
    global numberOfGenes
    for x in range(numberOfGenes):
        if individual[x] == 0:
            fitness += 1
    return fitness

def sumOfOnesMax(individual):
    fitness = 0
    global numberOfGenes
    for x in range(numberOfGenes):
        if individual[x] == 1:
            fitness += 1
    return fitness

def randomNumbers(individual):
    fitness = 0
    global numberOfGenes
    for x in range(numberOfGenes):
        if individual[x] == 0:
            fitness += 1
    return fitness

def semanticOptimisation(chromosome):
        # Add one to fitness if the character in the individual is the same, and in same position, as target sentence
        fitness = 0
        global numberOfGenes
        for x in range(numberOfGenes):
            if chromosome[x] == semanticPhrase[x]:
                fitness += 1
        return fitness

def calculateFitnessScore(individual):
    switcher = {
        1: sumOfOnesMin,
        2: sumOfOnesMax,
        3: semanticOptimisation,
        4: randomNumbers
    }
    optimise = switcher.get(whichProblem)
    return optimise(individual)

'''
Functions related to the initialisation and handling of the genetic algorithm
'''
def generate_population(size):
    currentPopulation = []

    global numberOfGenes

    # Setting the bounds for the problem (sumOfOnes, takes 0 or 1, binary)
    global upperBound
    global lowerBound

    global optimalNumber

    if whichProblem == 4:
        y = numberOfGenes
        for x in range(0, y):
            slot = random.randint(lowerBound, upperBound)
            optimalNumber.append(slot)

    # Loop to populate
    for x in range(size):
        '''
        Size is parsed as the predefined POPULATION_SIZE variable

        The chromosome is the individual, the data type is initialised as a list
        The design of this is so that it is not bound by data type when the same code is reused for different combinatorial problems

        Each Individual/Chromosome has a list of randomly chosen genes
        The number of genes per chromosome has a predefined dependency on (numberOfGenes)
        '''
        chromosome = []

        if whichProblem == 3:
            for y in range(len(semanticPhrase)):
                chromosome.append(random.choice(
                    string.ascii_lowercase))  # https://www.codegrepper.com/code-examples/python/how+to+pick+random+lowercase+alphabets+in+python
                currentPopulation.append(chromosome)


        else:
            # Populating random binary values for each gene in the chromosome/individual
            for y in range(numberOfGenes):
                chromosome.append(random.randint(lowerBound, upperBound))
            # print("Debug")

            currentPopulation.append(chromosome)
    return currentPopulation

def runGA(population):
    # List that stores the best fitness in the current population for each generation
    generationalFitness = []

    global GENERATIONS
    global solutionFound
    global finalGenerationCount

    currentGenerationNumber = 1
    while solutionFound != True and GENERATIONS > currentGenerationNumber:
        '''
        fitScores: List to store the returned fitness scores for each individual in the current population
        This list can be sorted by Max or Min to return the best and worst performing chromosomes for each generation
        '''
        fitScores = []

        '''
        Loop through the population and calculate the fitness for each chromosome
        This depends on the problem that I am trying to solve
        Chromosome is passed to the compute_fitness function 
        '''
        for x in population:
            #print("Testing this out")
            returnedFitness = calculateFitnessScore(x)
            fitScores.append(returnedFitness)
            #print("debug")

            if whichProblem == 1 or 2 or 3 or 4:
                if returnedFitness == numberOfGenes:
                    #print ("The solution was found early in generation number", currentGenerationNumber)
                    finalGenerationCount = currentGenerationNumber
                    solutionFound = True  # This would break the loop
                    currentBestFitness = max(fitScores)
                    index = fitScores.index(currentBestFitness)
                    generationsBestChromosome = population[index]
                    return generationsBestChromosome

        currentBestFitness = max(fitScores)
        generationalFitness.append(currentBestFitness)
        index = fitScores.index(currentBestFitness)
        generationsBestChromosome = population[index]
        # (generationsBestChromosome)
        

        '''
        If the solution has not been found in this generation 
        Or the max limit of generations has not been reached
        Call next_generation: Which does the following
        - Selection
        - Crossover
        - Mutation
        '''




        population = breedNextGeneration(population, fitScores)
        currentGenerationNumber += 1
    # Get the best solution


    '''
    generationalFitness is updated each time the fitness has been computed for each individual in the population
    generationalFitness will be a list with the best fitness at each generation stored
    This is useful for visualising the data, to show how quickly the GA converged or diverged from the solution
    ^^ As predictable results are not guarenteed with evolutionary computing
    '''

    # Return the position of the best individual in the population


    return generationsBestChromosome

def breedNextGeneration(population, fitness):
    global POPULATION_SIZE

    # Next Generation
    next_generation = []

    #print ("Debug")
    for _ in range(int(POPULATION_SIZE / 2)):
        # Perform parent selection
        parentOne = selection(population, fitness)
        parentTwo = selection(population, fitness)
        # Creat children using selected parents
        next_generation = crossover(parentOne, parentTwo, next_generation)
    # Perform mutation on next generation

    for x in range(len(next_generation)):
        next_generation[x] = mutation(next_generation[x])

    #print("Debug")
    return next_generation

def selection(previous_population, fitness):

    global SELECTION_METHOD

    if SELECTION_METHOD == 1:

        # Tournament selection with 4 randomly selected individuals
        candidate1 = fitness[random.randint(0, POPULATION_SIZE - 1)]
        candidate2 = fitness[random.randint(0, POPULATION_SIZE - 1)]

        candidate3 = fitness[random.randint(0, POPULATION_SIZE - 1)]
        candidate4 = fitness[random.randint(0, POPULATION_SIZE - 1)]

        # The candidates at this point are only indexes in the array to the position of the chromosomes who have been selected in the tournement
        # To do a compute fitness function I need to return the actual individual, not just the index
        #while candidate1 == candidate2:
            #candidate2 = previous_population[random.randint(0, POPULATION_SIZE)]

        #while candidate3 == candidate4:
            #candidate4 = previous_population[random.randint(0, POPULATION_SIZE)]


        fighter1 = previous_population[candidate1]
        candidate1 = calculateFitnessScore(fighter1)

        fighter2 = previous_population[candidate2]
        candidate2 = calculateFitnessScore(fighter2)

        fighter3 = previous_population[candidate3]
        candidate3 = calculateFitnessScore(fighter3)

        fighter4 = previous_population[candidate4]
        candidate4 = calculateFitnessScore(fighter4)


        # Bias towards the first comparrison in the case of equal fitness
        if (candidate1 >= candidate2):
            firstFinalist = fighter1
            finalist1Score = candidate1
        else:
            firstFinalist = fighter2
            finalist1Score = candidate2

        # Bias towards the first comparrison in the case of equal fitness
        if (candidate3 >= candidate4):
            secondFinalist = fighter3
            finalist2Score = candidate3
        else:
            secondFinalist = fighter4
            finalist2Score = candidate4

        # Bias towards the first comparrison in the case of equal fitness
        if (finalist1Score >= finalist2Score):
            return firstFinalist
        else:
            return secondFinalist

def crossover(parent1, parent2, next_generation):
    global numberOfGenes
    global crossoverMethod
    global CROSSOVER_RATE
    '''
    If CROSSOVER_CHOICE is 1, this is going to be random single point crossover
    If CROSSOVER_CHOICE is 2, this is going to be random multiple point crossover
    Show debug in the report so that we have the variables for the 3 segments of the crossover
    '''

    crossoverPoint = random.randint(0, (numberOfGenes - 1))

    chanceOfBreeding = random.random()

    if crossoverMethod == 1:
        if chanceOfBreeding < CROSSOVER_RATE:
            offspring1 = list(parent1[:crossoverPoint] + parent2[crossoverPoint:])
            offspring2 = list(parent2[:crossoverPoint] + parent1[crossoverPoint:])
            next_generation.append(offspring1)
            next_generation.append(offspring2)
            return next_generation
        else:
            next_generation.append(parent1)
            next_generation.append(parent2)
            return next_generation

    if crossoverMethod == 2:
        if chanceOfBreeding < CROSSOVER_RATE:
            crosspoint1 = random.randint(0, numberOfGenes)
            crosspoint2 = random.randint(0, numberOfGenes)

            # Check to ensure its a multiple point cross over, and not a single point crossover
            offspring1 = list(parent1[:crosspoint1] + parent2[crosspoint1:crosspoint2] + parent1[crosspoint2:])
            offspring2 = list(parent1[:crosspoint1] + parent2[crosspoint1:crosspoint2] + parent1[crosspoint2:])
            next_generation.append(offspring1)
            next_generation.append(offspring2)
            return next_generation
        else:
            next_generation.append(parent1)
            next_generation.append(parent2)
            return next_generation

def mutation(chromosome):
    global MUTATION_RATE
    global mutationMethod
    global lowerBound
    global upperBound
    global numberOfGenes

    if random.random() < MUTATION_RATE:
        if mutationMethod == 1:
            '''
            Mutation method 1 = Swap Mutation 
            https://www.tutorialspoint.com/genetic_algorithms/genetic_algorithms_mutation.htm
            '''
        position1 = random.randint(0, numberOfGenes - 1)
        position2 = random.randint(0, numberOfGenes - 1)


        # Check to see that the same position is not chosen twice
        while position1 == position2:
            position2 = random.randint(0, numberOfGenes - 1)

        # Swapping the positions around
        x = chromosome[position1]
        y = chromosome[position2]
        chromosome[position1] = y
        chromosome[position2] = x

        return chromosome
    return chromosome

def main():
    global POPULATION_SIZE
    global GENERATIONS

    global numberOfGenes
    global optimalNumber
    global lowerBound
    global upperBound

    global semanticPhrase
    global finalGenerationCount
    global optimalNumber


    if whichProblem == 1:
        # Sum 1s Minimisation
        lowerBound = 0
        upperBound = 1
        population = generate_population(POPULATION_SIZE)
        # Plot genetic algorithm results


        solution = runGA(population)

        '''
        Do the final wrapping up for debugging here
        
        If broke early (so global generation count is greater than 0)
            - Print, the solution was found before the generation cap
            - The solution was found in generation (global)
                - The solution was ("best individual")
        
        Else: 
            - The algorithm reached its maximum generation limit of (Reference the generation limit)
            - The best individual found was
                - Print the best individual (and the generation number, to make things easy, copy it straight over)
        '''

        if finalGenerationCount > -1:
            print ("The optimal solution was found at generation", finalGenerationCount, "\n")
            print ("The fittest individual was", solution)

        if finalGenerationCount == -1:
            print ("The genetic algorithm ran for", GENERATIONS, "generations \n")
            print ("The fittest individual was", solution)

    if whichProblem == 2:
        lowerBound = 0
        upperBound = 1
        population = generate_population(POPULATION_SIZE)
        solution = runGA(population)

        if finalGenerationCount > -1:
            print("The optimal solution was found at generation", finalGenerationCount, "\n")
            print("The fittest individual was", solution)

        if finalGenerationCount == -1:
            print("The genetic algorithm ran for", GENERATIONS, "generations \n")
            print("The fittest individual was", solution)
    if whichProblem == 3:
        numberOfGenes = len(semanticPhrase)
        '''
        No need for lower and upper bounds as I am deadling with character values
        I am only generating alphabet characters, not complex characters
        '''

        population = generate_population(POPULATION_SIZE)
        solution = runGA(population)
        howCloseWasString = calculateFitnessScore(solution)

        if finalGenerationCount > -1:
            print("The optimal solution was found at generation", finalGenerationCount, "\n")
            print("The fittest individual was", solution)
            print("The fitness of the individual was", howCloseWasString)

        if finalGenerationCount == -1:
            print("The genetic algorithm ran for", GENERATIONS, "generations \n")
            print("The fittest individual was", solution, "\n")
            print("The fitness of the individual was", howCloseWasString)

        print("Debug")
    if whichProblem == 4:
        lowerBound = 0
        upperBound = 9

        population = generate_population(POPULATION_SIZE)
        solution = runGA(population)

        howCloseWasNumber = calculateFitnessScore(solution)
        print ("The target number was:", optimalNumber, "\n")
        if finalGenerationCount > -1:
            print("The optimal solution was found at generation", finalGenerationCount, "\n")
            print("The fittest individual was", solution)
            print("The fitness of the individual was", howCloseWasNumber)

        if finalGenerationCount == -1:
            print("The genetic algorithm ran for", GENERATIONS, "generations \n")
            print("The fittest individual was", solution, "\n")
            print("The fitness of the individual was", howCloseWasNumber)

if __name__ == '__main__':
    main()
