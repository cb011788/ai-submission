# -*- coding: utf-8 -*-

# Use standard python package only.
import random
import string
import matplotlib.pyplot as plt
import time
import pandas as pd
import math

# Execution/Code verification requirements
'''
The following values for WhichProblem correspond to the optimisation problem my Genetic Algorithm will compute
5 = Sum of Squares
6 = Griewank
7 = Trid
8 = Rastrigen
9 = Sphere
10 = Schwefel
'''

whichProblem = 10
'''
The following values for TestBench correspond to the independent variables, where.....
1 = Population Size
2 = Crossover Rate
3 = Mutation Rate
4 = Maximum Generations
5 = Selection Method (Tournement or Ranked Roulette)
6 = Crossover Method (Random Single point or Random Multiple Point)
'''
TestBench = 0

# Varun variables
POPULATION_SIZE = 60
# 101 = 100 Generations, due to the way I have coded the incrementation
GENERATIONS = 101
CROSSOVER_RATE = 0.8
MUTATION_RATE = 0.2

'''
Selection Method 1 = Tournament (Best method)
Selection Method 2 = Ranked Selection
'''
SELECTION_METHOD = 2

# Kai variations
lowerBound = 0
upperBound = 1
numberOfGenes = 6

'''
crossoverMethod 1 = Single Point Crossover
crossoverMethod 2 = Multiple Point Crossover
'''
mutationMethod = 1
crossoverMethod = 1


finalGenerationCount = -1
resultsGeneration = []
resultsFitness = []

solutionFound = False

# Task 1.A
def sum_square(individual):
    currentTotal = 0
    for x in range(numberOfGenes):
        a = individual[x]
        b = a ** 2
        currentTotal += b

    return currentTotal


'''
Task 1.B - 5 more continuous optimisation problems
'''
# Function taken from: http://www-optima.amp.i.kyoto-u.ac.jp/member/student/hedar/Hedar_files/TestGO_files/Page1905.htm
def griewank(chromosome):
    currentTotal = 0
    for x in range(numberOfGenes):
        functionReturn = ((chromosome[x] ** 2) / 4000) - (math.cos(chromosome[x] / math.sqrt(x + 1)))
        currentTotal += functionReturn
    currentTotal += 1
    return currentTotal

# Function taken from: http://www-optima.amp.i.kyoto-u.ac.jp/member/student/hedar/Hedar_files/TestGO_files/Page2904.htm
def trid(chromosome):
    currentTotal = 0
    s1 = []
    s2 = []
    totalSum1 = 0
    totalSum2 = 0
    for x in range(0, numberOfGenes):
        s1.append((chromosome[x] - 1) ** 2)
    for y in range(1,numberOfGenes):
        s2.append((chromosome[x] * chromosome[x - 1]))

    for z in range(len(s1)):
        totalSum1 += s1[z]
    for w in range(len(s2)):
        totalSum2 += s2[w]

    currentTotal = totalSum1 - totalSum2
    return currentTotal

# Function taken from: http://www-optima.amp.i.kyoto-u.ac.jp/member/student/hedar/Hedar_files/TestGO_files/Page2607.htm
def rastrigen(chromosome):
    currentTotal = 0
    for x in range(numberOfGenes):
        squared = chromosome[x] ** 2
        currentTotal += (squared - 10) * math.cos(math.pi * (chromosome[x] * 2))
    currentTotal += (10 * numberOfGenes)
    return currentTotal

#4 Function taken from: http://www-optima.amp.i.kyoto-u.ac.jp/member/student/hedar/Hedar_files/TestGO_files/Page1113.htm
def sphere(chromosome):
    currentTotal = 0
    for x in range(numberOfGenes):
        currentTotal += (chromosome[x] ** 2)
    return currentTotal

#5 function taken from: http://www-optima.amp.i.kyoto-u.ac.jp/member/student/hedar/Hedar_files/TestGO_files/Page2530.htm
def schwefel(chromosome):
    currentTotal = 0
    z = 0
    for x in range(numberOfGenes):
        z += (chromosome[x] * math.sin(math.sqrt(abs(chromosome[x]))))
    # Rounded up to 5 decimal places
    currentTotal = ((418.49829 * numberOfGenes) - z)
    return currentTotal


def calculateFitnessScore(individual):
    switcher = {
        5: sum_square,
        6: griewank,
        7: trid,
        8: rastrigen,
        9: sphere,
        10: schwefel
    }
    problem = switcher.get(whichProblem)
    return problem(individual)
def checkSolution(bestGenerationalFitness):
    if whichProblem == 5 or 6 or 8 or 9 or 10:
        if bestGenerationalFitness == 0:
            return True
        else:
            return False
    # Trid has a global minima of -50 for 6 genes per chromosome
    if whichProblem == 7:
        if bestGenerationalFitness == -50:
            return True
        else:
            return False

def generate_population(size):
    currentPopulation = []

    global numberOfGenes


    global upperBound
    global lowerBound

    global optimalNumber

    # Loop to populate
    for x in range(size):
        '''
        Size is parsed as the predefined POPULATION_SIZE variable

        The chromosome is the individual, the data type is initialised as a list
        The design of this is so that it is not bound by data type when the same code is reused for different combinatorial problems

        Each Individual/Chromosome has a list of randomly chosen genes
        The number of genes per chromosome has a predefined dependency on (numberOfGenes)
        '''
        chromosome = []


        # Ignore this function, its from 2.B semantic, copied over to make the logic of this population generation work, this never gets executed
        if whichProblem == 3:
            for y in range(len(semanticPhrase)):
                chromosome.append(random.choice(
                    string.ascii_lowercase))  # https://www.codegrepper.com/code-examples/python/how+to+pick+random+lowercase+alphabets+in+python
                currentPopulation.append(chromosome)


        else:
            for y in range(numberOfGenes):
                chromosome.append(random.randint(lowerBound, upperBound))
            # print("Debug")

            currentPopulation.append(chromosome)
    return currentPopulation
#For whichProblem = 8 or 9: Handling of floating points, rather than integers
def generate_population2(size):
    currentPopulation = []

    global numberOfGenes


    global upperBound
    global lowerBound

    global optimalNumber

    # Loop to populate
    for x in range(size):
        '''
        Size is parsed as the predefined POPULATION_SIZE variable

        The chromosome is the individual, the data type is initialised as a list
        The design of this is so that it is not bound by data type when the same code is reused for different combinatorial problems

        Each Individual/Chromosome has a list of randomly chosen genes
        The number of genes per chromosome has a predefined dependency on (numberOfGenes)
        '''
        chromosome = []

        if whichProblem == 8 or 9:
            for y in range(numberOfGenes):
                chromosome.append(random.uniform(lowerBound, upperBound))
            currentPopulation.append(chromosome)


        else:

            for y in range(numberOfGenes):
                chromosome.append(random.randint(lowerBound, upperBound))
            # print("Debug")

            currentPopulation.append(chromosome)
    return currentPopulation

def runGA(population):
    # List that stores the best fitness in the current population for each generation
    generationalFitness = []

    global GENERATIONS
    global solutionFound
    global finalGenerationCount
    global resultsOutput

    currentGenerationNumber = 1

    # Main loop that handles the iterations for this program
    while solutionFound != True and GENERATIONS > currentGenerationNumber:
        '''
        fitScores: List to store the returned fitness scores for each individual in the current population
        This list can be sorted by Max or Min to return the best and worst performing chromosomes for each generation
        '''
        fitScores = []

        '''
        Loop through the population and calculate the fitness for each chromosome
        This depends on the problem that I am trying to solve
        Chromosome is passed to the compute_fitness function 
        '''
        for x in population:
            # print("Testing this out")
            returnedFitness = calculateFitnessScore(x)
            fitScores.append(returnedFitness)
            # print("debug")
            '''
            Check to see if the current chromosome in this loop is the solution
            '''
            '''
            if whichProblem == 1 or 2 or 3 or 4:
                if returnedFitness == numberOfGenes:
                    # print ("The solution was found early in generation number", currentGenerationNumber)
                    finalGenerationCount = currentGenerationNumber
                    solutionFound = True  # This would break the loop
                    currentBestFitness = max(fitScores)
                    index = fitScores.index(currentBestFitness)
                    generationsBestChromosome = population[index]
                    return generationsBestChromosome
            '''

        '''
        If the solution has not been found in this generation 
        Or the max limit of generations has not been reached
        Call next_generation: Which does the following
        - Selection
        - Crossover
        - Mutation
        '''

        '''
        Bubble sort the fitScores and the population
        This always sorts the population by fitness score
        Ascending order in terms of 'fitness'
        For a minimisation problem, lower fitness is better/closer to an optimum solution
        Best fitness is always population index [0]
        '''
        bubble_sort(fitScores, population)


        '''
        Print the best individual
        - Generation number
        - Individual // Chromosome genetic make up
        - The fitness score
        '''

        print('Generation number: ', currentGenerationNumber, '\n')
        print('Best chromosome in this generation is :  ', population[0], '\n')
        print('This individual had a fitness score of  ', fitScores[0], '\n')
        print('-------------------------------------------------------------')

        '''
        Save the best in each generation and generation number as something that I can print back out as a matplot graph
        '''

        '''
        Returns True if the optimal solution has been found in this generation
        Returns False if the optimal solution has not been found
        '''
        resultsFitness.append(fitScores[0])
        resultsGeneration.append(currentGenerationNumber)
        solutionFound = checkSolution(fitScores[0])
        #print('db')

        population = breedNextGeneration(population, fitScores)
        currentGenerationNumber += 1
    # Get the best solution


def breedNextGeneration(population, fitness):
    global POPULATION_SIZE

    # Next Generation
    next_generation = []

    # print ("Debug")
    for _ in range(int(POPULATION_SIZE / 2)):
        # Perform parent selection
        parentOne = selection(population, fitness)
        parentTwo = selection(population, fitness)
        # Creat children using selected parents
        next_generation = crossover(parentOne, parentTwo, next_generation)
    # Perform mutation on next generation

    for x in range(len(next_generation)):
        next_generation[x] = mutation(next_generation[x])

    return next_generation
def selection(previous_population, fitness):
    global SELECTION_METHOD

    if SELECTION_METHOD == 1:
        bubble_sort(previous_population, fitness)
        # Tournament selection with 4 randomly selected individuals
        fighter1 = previous_population[random.randint(0, POPULATION_SIZE - 1)]
        fighter2 = previous_population[random.randint(0, POPULATION_SIZE - 1)]

        fighter3 = previous_population[random.randint(0, POPULATION_SIZE - 1)]
        fighter4 = previous_population[random.randint(0, POPULATION_SIZE - 1)]

        # The candidates at this point are only indexes in the array to the position of the chromosomes who have been selected in the tournement
        # To do a compute fitness function I need to return the actual individual, not just the index
        # while candidate1 == candidate2:
        # candidate2 = previous_population[random.randint(0, POPULATION_SIZE)]

        # while candidate3 == candidate4:
        # candidate4 = previous_population[random.randint(0, POPULATION_SIZE)]

        candidate1 = calculateFitnessScore(fighter1)

        candidate2 = calculateFitnessScore(fighter2)

        candidate3 = calculateFitnessScore(fighter3)

        candidate4 = calculateFitnessScore(fighter4)

        # Bias towards the first comparrison in the case of equal fitness
        if (candidate1 <= candidate2):
            firstFinalist = fighter1
            finalist1Score = candidate1
        else:
            firstFinalist = fighter2
            finalist1Score = candidate2

        # Bias towards the first comparrison in the case of equal fitness
        if (candidate3 <= candidate4):
            secondFinalist = fighter3
            finalist2Score = candidate3
        else:
            secondFinalist = fighter4
            finalist2Score = candidate4

        # Bias towards the first comparrison in the case of equal fitness
        if (finalist1Score <= finalist2Score):
            return firstFinalist
        else:
            return secondFinalist

    if SELECTION_METHOD == 2:


        '''
        invertedFitness is for
        - Scaling
        - Handling divison by zero
        - weighted selection functions
        '''

        # As this is copied, currently has the same index as population, although this wil change
        invertedFitness = fitness.copy()
        # Smallest value from the original parsed fitness array
        scaler = min(invertedFitness)

        # Add the smallest value + one for each fitness value in the population
        for x in range (0, len(invertedFitness) - 1):
            a = invertedFitness[x]
            a = a + scaler + 1
            invertedFitness[x] = a
            a = 0


        # Provides an arbitrary total fitness number, used for weighted selection
        denominator = sum(invertedFitness)

        # This is so that smaller values have a higher chance of being picked in a weighted selection method
        completedInversion = []
        for x in range(0, len(invertedFitness)):
            # Invert the fitness, so that the new "Max" is the min values
            a = invertedFitness[x]
            try:
                y = (denominator / a)
            except ZeroDivisionError:
                y = 0

            completedInversion.append(y)

        # The above only returns the inverted fraction
        # Put the insertion sort to return the correct fraction here



        selectionDemoninator = sum(completedInversion)

        # Probabilty of each chromosome to be selected, depending on the fitness
        selectionNumerators = []

        # Make sure probability of selection in roulette wheel alligns with indexing of population
        for x in completedInversion:
            selectionNumerators.append(x/selectionDemoninator)

        # Debug check to see that the sum of all probabilities of being selected for breeding == 1.000000002(2 for binary//structural)
        #print(sum(selectionNumerators))

        bubble_sort(selectionNumerators, previous_population)

        '''
        Final part of the rank sort
        '''

        needle = random.uniform(0,max(selectionNumerators))
        rankCounter = -1
        selectionFound = False
        returnTest = []
        while selectionFound == False:
            for index in selectionNumerators:
                rankCounter += 1
                if index >= needle:
                    placemarker = previous_population[rankCounter]
                    returnTest.append(placemarker)
                    return previous_population[rankCounter]
                    selectionFound = True

def bubble_sort(our_list, population):
    # We go through the list as many times as there are elements
    for i in range(len(our_list)):
        # We want the last pair of adjacent elements to be (n-2, n-1)
        for j in range(len(our_list) - 1):
            if our_list[j] > our_list[j+1]:
                # Swap
                our_list[j], our_list[j+1] = our_list[j+1], our_list[j]
                population[j], population[j+1] = population[j+1], population[j]

def crossover(parent1, parent2, next_generation):
    global numberOfGenes
    global crossoverMethod
    global CROSSOVER_RATE
    '''
    If CROSSOVER_CHOICE is 1, this is going to be random single point crossover
    If CROSSOVER_CHOICE is 2, this is going to be random multiple point crossover
    Show debug in the report so that we have the variables for the 3 segments of the crossover
    '''

    crossoverPoint = random.randint(0, (numberOfGenes - 1))

    chanceOfBreeding = random.random()

    if crossoverMethod == 1:
        if chanceOfBreeding < CROSSOVER_RATE:
            offspring1 = list(parent1[:crossoverPoint] + list(parent2[crossoverPoint:]))
            offspring2 = list(parent2[:crossoverPoint] + list(parent1[crossoverPoint:]))
            next_generation.append(offspring1)
            next_generation.append(offspring2)
            return next_generation
        else:
            next_generation.append(parent1)
            next_generation.append(parent2)
            return next_generation

    if crossoverMethod == 2:
        if chanceOfBreeding < CROSSOVER_RATE:
            crosspoint1 = random.randint(0, numberOfGenes)
            crosspoint2 = random.randint(0, numberOfGenes)

            # Check to ensure its a multiple point cross over, and not a single point crossover
            offspring1 = list(parent1[:crosspoint1] + parent2[crosspoint1:crosspoint2] + parent1[crosspoint2:])
            offspring2 = list(parent1[:crosspoint1] + parent2[crosspoint1:crosspoint2] + parent1[crosspoint2:])
            next_generation.append(offspring1)
            next_generation.append(offspring2)
            return next_generation
        else:
            next_generation.append(parent1)
            next_generation.append(parent2)
            return next_generation
def mutation(chromosome):
    global MUTATION_RATE
    global mutationMethod
    global lowerBound
    global upperBound
    global numberOfGenes

    if random.random() < MUTATION_RATE:
        if mutationMethod == 1:
            '''
            Mutation method 1 = Swap Mutation 
            https://www.tutorialspoint.com/genetic_algorithms/genetic_algorithms_mutation.htm
            '''
        position1 = random.randint(0, numberOfGenes - 1)
        position2 = random.randint(0, numberOfGenes - 1)

        # Check to see that the same position is not chosen twice
        while position1 == position2:
            position2 = random.randint(0, numberOfGenes - 1)

        # Swapping the positions around
        x = chromosome[position1]
        y = chromosome[position2]
        chromosome[position1] = y
        chromosome[position2] = x

        return chromosome
    return chromosome

def main():
    global POPULATION_SIZE
    global GENERATIONS
    global CROSSOVER_RATE
    global MUTATION_RATE
    global SELECTION_METHOD

    global numberOfGenes
    global optimalNumber
    global lowerBound
    global upperBound

    global semanticPhrase
    global finalGenerationCount
    global optimalNumber

    global resultsGeneration
    global resultsFitness

    global TestBench

    global solutionFound


    '''
    if whichProblem == 5:
        lowerBound = -10
        upperBound = 10
        population = generate_population(POPULATION_SIZE)
        t0 = time.time()
        runGA(population)
        t1 = time.time()
        total = t1-t0
        print('The genetic algorithm took:  ', total, 'Seconds to execute', '\n')
        print('-----------------------------------------------------------------')

        testDataFrame = pd.DataFrame(columns=['Independent Variable', 'Value', 'Generations', 'Fitness Score', 'Execution Time'])
        testDataFrame = testDataFrame.append({'Independent Variable': 'Population', 'Value': POPULATION_SIZE, 'Generations': resultsGeneration[len(resultsGeneration)-1], 'Fitness Score': resultsFitness[len(resultsFitness) - 1], 'Execution Time': total}, ignore_index=True)
        #testDataFrame = testDataFrame.append({'Independent Variable': 'Population', 'Value': POPULATION_SIZE}, ignore_index=True)
        print(testDataFrame)
        plt.plot(resultsGeneration, resultsFitness)
        plt.show()
    '''
    # F1

    # Formula 2

    # Griewank: http://www-optima.amp.i.kyoto-u.ac.jp/member/student/hedar/Hedar_files/TestGO_files/Page1905.htm

    # Griewank
    if whichProblem == 5:
        lowerBound = -10
        upperBound = 10
        population = generate_population(POPULATION_SIZE)
        t0 = time.time()
        runGA(population)
        t1 = time.time()
        total = t1-t0
        print(total)
        plt.plot(resultsGeneration, resultsFitness)
        plt.ylabel("Fitness Score")
        plt.xlabel("Generations")
        plt.title("Sum of Squares")
        plt.show()

    if whichProblem == 6:
        lowerBound = -600
        upperBound = 600
        population = generate_population(POPULATION_SIZE)
        t0 = time.time()
        runGA(population)
        t1 = time.time()
        total = t1-t0
        print(total)
        plt.plot(resultsGeneration, resultsFitness)
        plt.ylabel("Fitness Score")
        plt.xlabel("Generations")
        plt.title("Griewank")
        plt.show()

    # Trid
    if whichProblem == 7:
        lowerBound = -20
        upperBound = 20
        population = generate_population(POPULATION_SIZE)
        t0 = time.time()
        runGA(population)
        t1 = time.time()
        total = t1-t0
        print(total)
        plt.plot(resultsGeneration, resultsFitness)
        plt.ylabel("Fitness Score")
        plt.xlabel("Generations")
        plt.title("Trid")
        plt.show()

    # Rastrigen and Sphere take -5.12 and 5.12 as their upper and lower bounds
    if whichProblem == 8:
        lowerBound = -5.12
        upperBound = 5.12
        population = generate_population2(POPULATION_SIZE)
        t0 = time.time()
        runGA(population)
        t1 = time.time()
        total = t1-t0
        print(total)
        plt.plot(resultsGeneration, resultsFitness)
        plt.ylabel("Fitness Score")
        plt.xlabel("Generations")
        plt.title("Rastrigen")
        plt.show()

    if whichProblem == 9:
        lowerBound = -5.12
        upperBound = 5.12
        population = generate_population2(POPULATION_SIZE)
        t0 = time.time()
        runGA(population)
        t1 = time.time()
        total = t1-t0
        print(total)
        plt.plot(resultsGeneration, resultsFitness)
        plt.ylabel("Fitness Score")
        plt.xlabel("Generations")
        plt.title("Sphere")
        plt.show()

    if whichProblem == 10:
        lowerBound = -500
        upperBound = 500
        population = generate_population(POPULATION_SIZE)
        t0 = time.time()
        runGA(population)
        t1 = time.time()
        total = t1-t0
        print(total)
        plt.plot(resultsGeneration, resultsFitness)
        plt.ylabel("Fitness Score")
        plt.xlabel("Generations")
        plt.title("Schwefel")
        plt.show()
    # Formula 4

    # Formula 5




    # Test bench for Population Size
    if whichProblem == 5 and TestBench == 1:
        PopulationTests = [40,80,120,160,200]
        PopulationFrame = pd.DataFrame(columns=['Independent Variable', 'Value', 'Generations', 'Fitness Score', 'Execution Time'])
        ResultsList = []


        # 5 test values
        for x in range(1,6):
            lowerBound = -10
            upperBound = 10
            # Test 5 times per test value
            for y in range(0,5):
                solutionFound = False
                testOutput = [0,0,0,0]
                population = generate_population(PopulationTests[x - 1])
                t0 = time.time()
                runGA(population)
                t1 = time.time()
                total = t1 - t0

                '''
                Save the variables to a list here (appending)
                '''
                testOutput[0] = PopulationTests[x - 1]
                testOutput[1] = resultsGeneration[len(resultsGeneration) - 1]
                testOutput[2] = resultsFitness[len(resultsFitness) - 1]
                testOutput[3] = total
                ResultsList.append(testOutput)
                print('db')

                #resultsGeneration = []
                #resultsFitness = []

        '''
        Push the list to the data frame here, to save the results
        '''
        # Pushing a list of lists into a pandas data frame: https://datascience.stackexchange.com/questions/26333/convert-a-list-of-lists-into-a-pandas-dataframe
        data = [[ResultsList[0]],[ResultsList[1]], [ResultsList[2]],[ResultsList[3]],[ResultsList[4]],[ResultsList[5]],[ResultsList[6]],[ResultsList[7]],[ResultsList[8]],[ResultsList[9]],[ResultsList[10]],[ResultsList[11]],[ResultsList[12]],[ResultsList[13]],[ResultsList[14]],[ResultsList[15]],[ResultsList[16]],[ResultsList[17]],[ResultsList[18]],[ResultsList[19]],[ResultsList[20]],[ResultsList[21]],[ResultsList[22]],[ResultsList[23]],[ResultsList[24]],]

        data = [ ResultsList[0], ResultsList[1], ResultsList[2], ResultsList[3], ResultsList[4],
                ResultsList[5], ResultsList[6], ResultsList[7], ResultsList[8], ResultsList[9],
                ResultsList[10], ResultsList[11], ResultsList[12], ResultsList[13], ResultsList[14],
                ResultsList[15], ResultsList[16], ResultsList[17], ResultsList[18], ResultsList[19],
                ResultsList[20], ResultsList[21], ResultsList[22], ResultsList[23], ResultsList[24]]

        TestBenchDF = pd.DataFrame.from_records(data, columns=["Population Size", "Generations", "Best Fitness Score", "Time taken to execute"])
        TestBenchDF.to_csv(r'/Users/kai/Documents\poptest8.csv', index=False)
        print(TestBenchDF)

    if whichProblem == 5 and TestBench == 2:
        lowerBound = -10
        upperBound = 10
        ResultsList = []
        for x in range(1,6):
            lowerBound = -10
            upperBound = 10
            CrossTesting = [0.1, 0.2, 0.4, 0.6, 0.8]
            # Test 5 times per test value
            for y in range(0,5):
                CROSSOVER_RATE = CrossTesting[x - 1]
                solutionFound = False
                testOutput = [0, 0, 0, 0]
                population = generate_population(POPULATION_SIZE)
                t0 = time.time()
                runGA(population)
                t1 = time.time()
                total = t1 - t0
                testOutput[0] = CrossTesting[x - 1]
                testOutput[1] = resultsGeneration[len(resultsGeneration) - 1]
                testOutput[2] = resultsFitness[len(resultsFitness) - 1]
                testOutput[3] = total
                ResultsList.append(testOutput)

        data = [ResultsList[0], ResultsList[1], ResultsList[2], ResultsList[3], ResultsList[4],
                ResultsList[5], ResultsList[6], ResultsList[7], ResultsList[8], ResultsList[9],
                ResultsList[10], ResultsList[11], ResultsList[12], ResultsList[13], ResultsList[14],
                ResultsList[15], ResultsList[16], ResultsList[17], ResultsList[18], ResultsList[19],
                ResultsList[20], ResultsList[21], ResultsList[22], ResultsList[23], ResultsList[24]]

        TestBenchDF = pd.DataFrame.from_records(data, columns=["Crossover Rate", "Generations", "Best Fitness Score",
                                                               "Time taken to execute"])
        TestBenchDF.to_csv(r'/Users/kai/Documents\crosstest1.csv', index=False)
        print(TestBenchDF)

    if whichProblem == 5 and TestBench == 3:
        lowerBound = -10
        upperBound = 10
        ResultsList = []
        for x in range(1,6):
            lowerBound = -10
            upperBound = 10
            MutationTesting = [0.3, 0.4, 0.5, 0.6, 0.7]
            # Test 5 times per test value
            for y in range(0,5):
                MUTATION_RATE = MutationTesting[x - 1]
                solutionFound = False
                testOutput = [0, 0, 0, 0]
                population = generate_population(POPULATION_SIZE)
                t0 = time.time()
                runGA(population)
                t1 = time.time()
                total = t1 - t0
                testOutput[0] = MutationTesting[x - 1]
                testOutput[1] = resultsGeneration[len(resultsGeneration) - 1]
                testOutput[2] = resultsFitness[len(resultsFitness) - 1]
                testOutput[3] = total
                ResultsList.append(testOutput)

        data = [ResultsList[0], ResultsList[1], ResultsList[2], ResultsList[3], ResultsList[4],
                ResultsList[5], ResultsList[6], ResultsList[7], ResultsList[8], ResultsList[9],
                ResultsList[10], ResultsList[11], ResultsList[12], ResultsList[13], ResultsList[14],
                ResultsList[15], ResultsList[16], ResultsList[17], ResultsList[18], ResultsList[19],
                ResultsList[20], ResultsList[21], ResultsList[22], ResultsList[23], ResultsList[24]]

        TestBenchDF = pd.DataFrame.from_records(data, columns=["Mutation Rate", "Generations", "Best Fitness Score",
                                                               "Time taken to execute"])
        TestBenchDF.to_csv(r'/Users/kai/Documents\MutationTest2.csv', index=False)
        print(TestBenchDF)

    if whichProblem == 5 and TestBench == 4:
        lowerBound = -10
        upperBound = 10
        ResultsList = []
        for x in range(1,6):
            lowerBound = -10
            upperBound = 10
            GenerationTesting = [50, 100, 150, 200, 250]
            # Test 5 times per test value
            for y in range(0,5):
                GENERATIONS = GenerationTesting[x - 1]
                solutionFound = False
                testOutput = [0, 0, 0, 0]
                population = generate_population(POPULATION_SIZE)
                t0 = time.time()
                runGA(population)
                t1 = time.time()
                total = t1 - t0
                testOutput[0] = GenerationTesting[x - 1]
                testOutput[1] = resultsGeneration[len(resultsGeneration) - 1]
                testOutput[2] = resultsFitness[len(resultsFitness) - 1]
                testOutput[3] = total
                ResultsList.append(testOutput)

        data = [ResultsList[0], ResultsList[1], ResultsList[2], ResultsList[3], ResultsList[4],
                ResultsList[5], ResultsList[6], ResultsList[7], ResultsList[8], ResultsList[9],
                ResultsList[10], ResultsList[11], ResultsList[12], ResultsList[13], ResultsList[14],
                ResultsList[15], ResultsList[16], ResultsList[17], ResultsList[18], ResultsList[19],
                ResultsList[20], ResultsList[21], ResultsList[22], ResultsList[23], ResultsList[24]]

        TestBenchDF = pd.DataFrame.from_records(data, columns=["Number of Generations", "Generations", "Best Fitness Score",
                                                               "Time taken to execute"])
        TestBenchDF.to_csv(r'/Users/kai/Documents\GenerationTesting1.csv', index=False)
        print(TestBenchDF)

    if whichProblem == 5 and TestBench == 5:
        lowerBound = -10
        upperBound = 10
        SelectionTesting = [1,2]
        ResultsList = []
        for x in range (1,3):
            for y in range(0,10):
                SELECTION_METHOD = SelectionTesting[x - 1]
                solutionFound = False
                testOutput = [0, 0, 0, 0]
                population = generate_population(POPULATION_SIZE)
                t0 = time.time()
                runGA(population)
                t1 = time.time()
                total = t1 - t0
                testOutput[0] = SelectionTesting[x - 1]
                testOutput[1] = resultsGeneration[len(resultsGeneration) - 1]
                testOutput[2] = resultsFitness[len(resultsFitness) - 1]
                testOutput[3] = total
                ResultsList.append(testOutput)

        data = [ResultsList[0], ResultsList[1], ResultsList[2], ResultsList[3], ResultsList[4],
                ResultsList[5], ResultsList[6], ResultsList[7], ResultsList[8], ResultsList[9],
                ResultsList[10], ResultsList[11], ResultsList[12], ResultsList[13], ResultsList[14],
                ResultsList[15], ResultsList[16], ResultsList[17], ResultsList[18], ResultsList[19]]

        TestBenchDF = pd.DataFrame.from_records(data,columns=["Selection Method", "Generations", "Best Fitness Score","Time taken to execute"])
        TestBenchDF.to_csv(r'/Users/kai/Documents\SelectionTesting2.csv', index=False)

    # Crossover Methods
    if whichProblem == 5 and TestBench == 6:
        lowerBound = -10
        upperBound = 10
        CrossoverTesting = [1, 2]
        ResultsList = []
        for x in range(1, 3):
            for y in range(0, 10):
                crossoverMethod = CrossoverTesting[x - 1]
                solutionFound = False
                testOutput = [0, 0, 0, 0]
                population = generate_population(POPULATION_SIZE)
                t0 = time.time()
                runGA(population)
                t1 = time.time()
                total = t1 - t0
                testOutput[0] = CrossoverTesting[x - 1]
                testOutput[1] = resultsGeneration[len(resultsGeneration) - 1]
                testOutput[2] = resultsFitness[len(resultsFitness) - 1]
                testOutput[3] = total
                ResultsList.append(testOutput)

        data = [ResultsList[0], ResultsList[1], ResultsList[2], ResultsList[3], ResultsList[4],
                ResultsList[5], ResultsList[6], ResultsList[7], ResultsList[8], ResultsList[9],
                ResultsList[10], ResultsList[11], ResultsList[12], ResultsList[13], ResultsList[14],
                ResultsList[15], ResultsList[16], ResultsList[17], ResultsList[18], ResultsList[19]]

        TestBenchDF = pd.DataFrame.from_records(data, columns=["Crossover Method", "Generations", "Best Fitness Score",
                                                               "Time taken to execute"])
        TestBenchDF.to_csv(r'/Users/kai/Documents\CrossoverTesting2.csv', index=False)


if __name__ == '__main__':
    main()
